// Problem 05:
//Write a Dart program that checks if a given number is even or odd and prints the result.
import 'dart:io';

void main() {
  stdout.write("Input: ");
  int input = int.parse(stdin.readLineSync()!);
  if (input % 2 == 0) {
    print('$input is even');
  } else {
    print('$input is Odd');
  }
}
