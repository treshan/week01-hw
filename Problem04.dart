// Problem 04:
//Write a Dart function that takes a list of numbers and returns a new list containing only the unique elements (no duplicates).
import 'dart:io';

void main() {
  List<int> elements = [];
  stdout.write('Enter how many number you want to input: ');
  int counters = int.parse(stdin.readLineSync()!);
  for (var i = 0; i < counters; i++) {
    stdout.write('List Items No. $i: ');
    int adds = int.parse(stdin.readLineSync()!);
    elements.add(adds);
  }
  List output = elements.toSet().toList();
  print('Output: $output');
}
