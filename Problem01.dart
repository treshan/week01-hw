// Problem 01:
//Write a Dart function that takes a list of numbers as input and returns the largest number in the list.
import 'dart:io';

void main() {
  List<int> elements = [];
  stdout.write('Enter how many number you want to input: ');
  int counters = int.parse(stdin.readLineSync()!);
  for (var i = 0; i < counters; i++) {
    stdout.write('List Items No. $i: ');
    int adds = int.parse(stdin.readLineSync()!);
    elements.add(adds);
  }
  var largestValue = elements[0];
  for (var i = 0; i < elements.length; i++) {
    // Checking for largest value in the list
    if (elements[i] > largestValue) {
      largestValue = elements[i];
    }
  }
  print('Input: $elements');
  print('Output: $largestValue');
}
