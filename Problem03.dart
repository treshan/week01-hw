// Problem 03:
//Write a Dart function that generates the first n numbers in the Fibonacci sequence and returns them in a list.
import 'dart:io';

void main() {
  stdout.write('Input: ');
  int counter = int.parse(stdin.readLineSync()!);
  List<int> output = [];
  int fibonacci(int n) {
    if (n == 0 || n == 1) {
      return n;
    }
    return fibonacci(n - 1) + fibonacci(n - 2);
  }

  for (int i = 0; i < counter; i++) {
    output.add(fibonacci(i));
  }
  print('Output: $output');
}
