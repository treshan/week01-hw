// Problem 06:
//Write a Dart program that calculates and prints the factorial of a given positive integer.
import 'dart:io';

void main() {
  stdout.write("Input: ");
  int input = int.parse(stdin.readLineSync()!);
  int output = 1;
  for (int i = 1; i <= input; i++) {
    output *= i;
  }
  print('Output: $output');
}
